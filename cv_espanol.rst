Joaquin Tomás de Andres y Martinez de Arenasa
=============================================

-----

Información de Contacto
-----------------------

| Nacionalidad: Argentino
| Fecha de nacimiento: 30 de Marzo de 1984
| Telegram: @xcancerberox
| Email: xcancerberox@gmail.com
| TEL: +54-11-23955742

| 

Sumario
-------

Soy un amante del software libre y lo que más me gusta es crear. Dedico el
tiempo que tengo a hacer, ya sea dentro de una empresa, en la comunidad o con
proyectos personales. Así es como también tengo muchos gustos por fuera del
trabajo como cerámica, carpintería, orfebrería, música entre otros, que me
llevan a estar creando todo el tiempo.

Soy un profesional (Ingeniéro Electrónico) con más de 10 años de diseño de
circuitos para distintas áreas (maquinaria industrial, sistemas de control,
educación, espacial, arte publicitario) y 8 años de desarrollo de software.
Cuento con vasta experiencia en el desarrollo de firmware para sistemas
embebidos sobre microcontroladores y microprocesadores, con y sin sistema
operativo. He desarrollado software de alto nivel en Python utilizando
frameworks como Django, así como he hecho puesta a punto de booteo de
microcontroladores en ASM.

| 

Experiencia Profesional
-----------------------

----

Desarrollo de sistemas embebidos para aplicaciones espaciales, Satellogic [Abril 2012 - Agosto 2016]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Desarrollo de computadora de abordo para la flota de satélites. Utilizando 
Sistem on Module basados en OMAP y microcontroladores ARM Cortex R4, diseñe una
computadora para control de los sistemas internos del satélite. Así también
desarrolle los circuitos para otros sistemas de la misma plataforma, su mayoría
basados en ARM Cortex R4.

Desarrollo del sistema seguro de booteo para los microcontroladores ARM Cortex
R4. En este caso fue necesario hacer una puesta a punto en ASM para lograr el
objetivo propuesto.

Desarrollo de aplicaciones para FreeRTOS. Debuging utilizando openOCD y GDB.

Dentro de esta empresa también pude aportar trabajando como team leader del
equipo de electrónica y sistemas embebidos, llevando a cabo las tareas de
coordinación y generación de proyectos, interacción y relevamiento de
necesidades de otros equipos, manejo de recursos, desarrollo de proyectos,
testeo de producto, puesta a punto y mantenimiento.

Tecnologías utilizadas:

- FreeRTOS
- Linux
- C, ARM ASM, Python, VHDL, Verilog
- OpenOCD + GDB
- Altium
- NGSpice
- Gitlab, Gitlab CI
- Grafana + InfluxDB

Desarrollo de soluciones electrónicas
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

En forma independiente a lo largo de 10 años desarrolle circuitos para
solución de problemas específicos tratando con clientes y relevando
requerimientos, llevando los proyectos desde su creación hasta la
implementación del producto.

La mayoría de los desarrollos independientes fueron entregados al cliente con
licencia Creative Commons o MIT.

Tecnologías utilizadas:

- Linux
- C, AVR ASM, 8051 ASM, Python
- MongoDB
- Kicad 
- NGSpice
- Git, Mercurial, Darcs


Trabajos en seguridad informática
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Durante 3 años, entre 2007 y 2010, trabajé haciendo auditorías de seguridad
informática y pentesting a distintas empresas, contratado por la consultora
 Insside.

Algunas de las empresas a las que brindé servicios:

- Standard Bank
- YPF
- Total


Trabajos de docencia
^^^^^^^^^^^^^^^^^^^^

Siendo una de mis preferencias, me dediqué a la docencia siempre que tuve
oportunidad trabajando así como ayudante en la facultad y dictando cursos
propios posteriormente.

Algunos cursos relevantes dictados:

- Carrera de Posgrado en sistemas embebidos en la Universidad Técnica Nacional (UTN).
- Carrera de Posgrado en sistemas embebidos la Facultad de Ingeniería, UBA.
- Curso de ``Linux Embebido desde cero`` para la empresa Itaipu.
- Curso de ``Linux Embebido desde cero`` para la empresa INVAP.
- Curso de programación de SoM MBED en el marco del SASE.

Trabajos en Redes y Linux
^^^^^^^^^^^^^^^^^^^^^^^^^

En el período entre 2001 y 2007, siendo mis primeros trabajos, me dedique a la
administración de redes y servidores Linux.

Algunas empresas con las que trabaje:

- China Shipping Argentina
- Universidad de Buenos Aires, Facultad de ingeniería, Laboratorio Abierto
- Informar Computación.

----

Sumario de conocimientos
------------------------

- Electrónica

  - Diseño de circuitos basados en microcontroladores ARM.
  - Diseño de circuitos basados en SoM.
  - Diseño de circuitos tolerantes a falla.
  - Diseño de circuitos con FPGA.
  - Simulación de circuitos analógicos (NGSPICE). 
  - Aplicaciones CAD para desarrollo de circuitos (Kicad, Altium)

- Software

  - Especializado en lenguajes: C, Python, AVR assembly, ARM assembly, 8051 assembly.
  - Otros lenguajes utilizados: C++, Rapid, VHDL, Verilog.  
  - Linux from scratch:

    - Openembedded
    - Buildroot
    - Crosstool-ng

  - FreeRTOS
  - Linux embebido con sistemas operativos Debian y Arch
  - Herramientas de debuging (OpenOCD + GDB)

| 

Cursos
------
- Escuela Argentina de Micro-nanoelectrónica, Tecnología y Aplicaciones (EAMTA) 2017. Julio 2017.
- International School on the Effects of Radiation on Embedded Systems for Space Applications. Diciembre 2014.
- Escuela Argentina de Microelectrónica, Tecnología y Aplicaciones (EAMTA), Agosto 2011.
- Introducción a las Aplicaciones Industriales del Procesamiento Digital de Datos, ECI 2011, Facultade de Ciencias Exactas, UBA.
- Introduction to Mobile Robotics, ECI 2011, Facultade de Ciencias Exactas, UBA.
- ICTP-Latin American Basic Course on FPGA Design for Scientific Instrumentation.
- Administración de servicios de redes GNU/Linux dictado en la Facultad de Ingeniería de la UBA.
- Configuración y Administración de sistemas GNU/Linux dictado en la Facultad de Ingeniería de la UBA.
- Básico de Redes Cisco, dictado por la misma empresa.

| 

Idiomas
-------
- Español: Lengua materna.
- Inglés: Intermedio.

| 

Actividades en la comunidad
---------------------------
- Miembro y socio de Python Argentina.
- Desarrollador activo del equipo "Debian Salsa CI".
- Miembro del grupo de usuarios de GNU/Linux de la Facultad de Ingeniería (LugFi).
- Miembro del grupo de robótica de la Facultad de ingeniería. 
