Joaquin Tomás de Andres and Martinez de Arenasa
===============================================

| Date of birth: March 30, 1984
| Telegram: @xcancerberox
| Email: xcancerberox@gmail.com
| Gitlab/github: @xcancerberox
| Debian Salsa: @xcancerberox-guest
| Phone: +34 697 683342

|

Summary
-------

I am a lover of the free software philosophy, what I like most is to create.
I dedicate the time I have to it, either within a company, in the community or
with personal projects.

I am a professional (Electronics Engineer) with more than 10 years of
experience in circuit design for different areas (industrial machinery, control
systems, space, education, advertising) and 8 years of software development in
the same areas. I have vast experience in the development of firmware for
embedded systems on microcontrollers and microprocessors, with and without
operating system. My experience covers from low level application like fine
tuning of ARM bootloaders in ASM, to high level application using frameworks
like Django.

I have vast experience using and administrating GNU/Linux systems. Also, I
have vast experience deploying and testing using containers technologies like
docker and LXC.
|

----

Professional experience
-----------------------

Development of embedded systems for space applications, Satellogic [April 2012 - August 2018]
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On-board computer development of the satellite fleet. Using systems based on
ARM Cortex A, R and M profiles. I have designed the main computer dedicated to
control of internal satellite systems. Also, I have developed the circuits for
other systems on the same platform, mostly based on ARM Cortex R4.

Development of a safe booting system for ARM Cortex R4 microcontrollers. In
this case it was necessary to make a set-up in ASM to achieve the proposed
objective respecting its critical constraints.

Development of applications for FreeRTOS. Debuging using OpenOCD and GDB.

I worked as a team leader of the
electronic and embedded systems team, carrying out the tasks of
coordination and generation of projects, interaction and survey of
needs for other teams, resource management, project development,
product testing, commissioning and maintenance.

Used technology:

- FreeRTOS
- Linux
- C, ARM ASM, Python, VHDL, Verilog
- OpenOCD + GDB
- Altium
- NGSpice
- Gitlab, Gitlab CI
- Grafana + InfluxDB
- Docker

Development of electronic solutions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Independently, over 12 years, I have developed circuits for solution of specific
problems dealing with clients and relieving requirements, taking the projects
from their creation to the product implementation.

Most of the independent developments were delivered to the customer with
Creative Commons or MIT license.

Used technology:

- Linux
- C, AVR ASM, 8051 ASM, Python
- MongoDB
- Kicad
- NGSpice
- Git, Mercurial
- Docker


IT and security
^^^^^^^^^^^^^^^

For 3 years, between 2007 and 2010, I worked doing security audits
and pentesting to different companies, hired as consultant by
Insside.

Some of the companies to which I provided these services:

- Standard Bank
- YPF
- Total


Teaching
^^^^^^^^

Being one of my preferences, I dedicated myself to teach whenever I had the
opportunity. I worked as assistant in the University of Buenos Aires, and
also teach courses developed with a colleague.

Some relevant courses taught:

- Postgraduate degree in embedded systems at the Argentinian National Technical University (UTN).
- Postgraduate degree in embedded systems at the Department of Engineering, University of Buenos Aires.
- Course of `` Embedded Linux from scratch '' for Itaipu.
- Course of `` Embedded Linux from scratch '' for INVAP SE.
- MBED System-on-Module programming course in the SASE conference.

Jobs in Networking and Linux
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the period between 2001 and 2007, being my first jobs, I dedicated myself to
Linux networking and server administration.

Some companies I worked with:

- China Shipping Argentina
- University of Buenos Aires, Department of Engineering, Open Laboratory
- Informar Computación.

----

Summary of knowledges
---------------------

- Electronics

  - Design of circuits based on ARM microcontrollers.
  - Design of circuits based on ARM System-on-Module.
  - Design of circuits based on AVR microcontrollers.
  - Design of fault tolerant circuits.
  - Circuit design with FPGA.
  - Simulation of analog circuits (NGSPICE).
  - CAD applications for circuit development (Kicad, Altium)

- Software

  - Specialized in languages: C, Python, AVR assembly, ARM assembly, 8051 assembly.
  - Other languages used: C++, Rapid, VHDL, Verilog.
  - Linux from scratch:

    - Openembedded
    - Buildroot
    - Crosstool-ng

  - FreeRTOS
  - Embedded Linux
  - Debuging tools (OpenOCD + GDB)
  - Docker
  - LXC

|

Courses
-------
- Argentinian School of Microelectronics, Technology and Applications (EAMTA) 2017. July 2017.
- International School on the Effects of Radiation on Embedded Systems for Space Applications. December 2014
- Argentinian School of Microelectronics, Technology and Applications (EAMTA), August 2011.
- Introduction to Industrial Applications of Digital Data Processing, ECI 2011, Department of Exact Sciences, UBA.
- Introduction to Mobile Robotics, ECI 2011, Department of Exact Sciences, UBA.
- ICTP-Latin American Basic Course on FPGA Design for Scientific Instrumentation.
- Administration of GNU/Linux network services taught at the UBA School of Engineering.
- Configuration and Administration of GNU/Linux systems dictated in the Department of Engineering of the UBA.
- Cisco Network Basic, dictated by the same company.

|

Languages
---------

- Spanish: Mother tongue.
- Intermediate English.

|

Community activities
--------------------
- Active developer of the "Debian Salsa CI" team.
- Member and partner of Python Argentina.
- Member of the GNU/Linux user group of the University of Buenos Aires, department of Engineering (LugFi).
- Member of the robotics group of the University of Buenos Aires, department of Engineering.
